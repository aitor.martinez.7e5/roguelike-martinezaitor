using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private bool isExploding;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private float enemy_speed = 1f;
    private float enemy_hp = 60;

    // Start is called before the first frame update
    void Start()
    {
        isExploding = false;

    }

    // Update is called once per frame
    void Update()
    {
        EnemyMovement();
    }






    void EnemyMovement()
    {
        var last_position = transform.position.x;
        if (isExploding == false)
        {
         transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("Player").transform.position, enemy_speed * Time.deltaTime);

        }
        if (GameObject.Find("Player").transform.position.x < last_position)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }

      
    }



    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("bullet"))
        {
            Destroy(collision.gameObject);
            enemy_hp = enemy_hp - GameObject.Find("Player").GetComponent<PlayerController>().attack_damage;
        }
        if (collision.CompareTag("bullet")&&enemy_hp< GameObject.Find("Player").GetComponent<PlayerController>().attack_damage)
        {
            Destroy(gameObject);
            GameObject.Find("HUD").GetComponent<HUDManager>().IncreaseScore(10);
            if (collision.CompareTag("bullet"))
            {
                Destroy(collision.gameObject);
            }
        }
       

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            GetComponent<BoxCollider2D>().enabled=false;
            Invoke("Attack", 1f);
            

            animator.SetBool("isNextToThePlayer", true);

        }
    }

    private void Attack()
    {
        GameObject.Find("HUD").GetComponent<HUDManager>().ReceiveDamage();
        Destroy(gameObject);
    }
    

    /*void CheckIfOut()
    {
        if (transform.position.y >= 9.5)
        {
            Destroy(gameObject);
        }
    }*/
}
