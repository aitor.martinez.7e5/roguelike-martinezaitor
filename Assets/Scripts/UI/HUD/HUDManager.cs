using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class HUDManager : MonoBehaviour
{
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text ammo;
    private double minutes= 00;
    private double seconds= 00;
    [SerializeField]
    private Text contador;
    private float score;
    private float hp = 90;
    [SerializeField]
    private GameObject hp_bar;
    private Image img;
    [SerializeField]
    private Sprite[] sprites;

    // Start is called before the firdst frame update
    void Start()
    {
       
        img = hp_bar.GetComponent<Image>();
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        HP();
        scoreText.text = $"{score}";
        seconds += Time.deltaTime;
        if (seconds >= 59)
        {
            minutes++;
            seconds = 00;
        }

        string auxMin;
        string auxSec;

        if (seconds <= 9) auxSec = "0" + Mathf.Round((float)seconds).ToString();
        else auxSec = Mathf.Round((float)seconds).ToString();

        if (minutes <= 9) auxMin = "0" + minutes.ToString();
        else auxMin = minutes.ToString();
        if (GameObject.Find("Player").GetComponent<PlayerController>()._indexWeapon == 0)
        {
            ammo.GetComponent<Text>().text = GameObject.Find("gun").GetComponent<SMG_Controller>().actual_ammo + "/" + GameObject.Find("gun").GetComponent<SMG_Controller>().max_ammo;
        }
        if (GameObject.Find("Player").GetComponent<PlayerController>()._indexWeapon == 1)
        {
            ammo.GetComponent<Text>().text = GameObject.Find("MiniGun").GetComponent<MiniGun_Controller>().actual_ammo + "/" + GameObject.Find("MiniGun").GetComponent<MiniGun_Controller>().max_ammo;
        }
        if (GameObject.Find("Player").GetComponent<PlayerController>()._indexWeapon == 2)
        {
            ammo.GetComponent<Text>().text = GameObject.Find("Sniper").GetComponent<SNIPER_Controller>().actual_ammo + "/" + GameObject.Find("Sniper").GetComponent<SNIPER_Controller>().max_ammo;
        }
        if (GameObject.Find("Player").GetComponent<PlayerController>()._indexWeapon == 3)
        {
            ammo.GetComponent<Text>().text = GameObject.Find("AK-47").GetComponent<AK47_Controller>().actual_ammo + "/" + GameObject.Find("AK-47").GetComponent<AK47_Controller>().max_ammo;
        }



        contador.GetComponent<Text>().text = auxMin + ":" + auxSec ;
        
        /*
        if (seconds <= 9) contador.GetComponent<Text>().text = "0" + Mathf.Round((float)minutes).ToString() + ":" + "0" + Mathf.Round((float)seconds).ToString();
        else if (seconds<=9&&minutes<=10) contador.GetComponent<Text>().text = "0" + Mathf.Round((float)minutes).ToString() + ":" + "0" + Mathf.Round((float)seconds).ToString();
        else if (minutes <= 10) contador.GetComponent<Text>().text = "0" + Mathf.Round((float)minutes).ToString() + ":" + "0" + Mathf.Round((float)seconds).ToString();
        else contador.GetComponent<Text>().text = Mathf.Round((float)minutes).ToString() + ":" + Mathf.Round((float)seconds).ToString();
       // if (seconds < 0) SceneManager.LoadScene("Game1"); */
    }
    
    public void IncreaseScore(float AddScore)
    {
        score += AddScore;
    }

    public void ReceiveDamage()
    {
        hp = hp - 10;
        Debug.Log(hp);
    }

    public void HP()
    {
        switch (hp)
        {
            case 90:
                img.sprite = sprites[9];
                break;

            case 80:
                img.sprite = sprites[8];
                break;

            case 70:
                img.sprite = sprites[7];
                break;
            case 60:
                img.sprite = sprites[6];
                break;
            case 50:
                img.sprite = sprites[5];
                break;
            case 40:
                img.sprite = sprites[4];
                break;
            case 30:
                img.sprite = sprites[3];
                break;
            case 20:
                img.sprite = sprites[2];
                break;
            case 10:
                img.sprite = sprites[1];
                break;
            case 0:
                img.sprite = sprites[0];
                GameObject.Find("MENU").GetComponent<MENUManager>().lose.SetActive(true);
                GameObject.Find("MENU").GetComponent<MENUManager>().isStarted=false;
                break;
        }



    }

    
}
