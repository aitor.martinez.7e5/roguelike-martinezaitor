using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemySpawner_Room1 : MonoBehaviour
{
   
    [SerializeField]
    private GameObject spawner_mark;
    [SerializeField]
    private float spawnTime = 1f;
    private float limit_enemies = 10;
    private int enemy_counter = 0 ;
    private bool onCooldown;


    // Start is called before the first frame update
    void Start()
    {
        onCooldown = false;
    }

    private void Update()
    {
            

        if (onCooldown == false&&enemy_counter!=limit_enemies&& GameObject.Find("MENU").GetComponent<MENUManager>().isStarted)
        {
           float newX = Random.Range(-10f, 10f);
           float newY = Random.Range(-5f, 5f);
           var instance1 = Instantiate(spawner_mark, new Vector3(newX, newY, 0), Quaternion.identity);
            enemy_counter++;
            onCooldown = true;
            Invoke("Cooldown", spawnTime);
         
        }
        
        

        /*if (enemy_counter == limit_enemies)
        {
            canSpawn = false;
            onCooldown = true;
            enemy_counter++;

        }*/
    }

    private void Cooldown()
    {
        onCooldown = false;
        
    }
    



   



}
