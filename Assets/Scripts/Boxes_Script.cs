using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxes_Script : MonoBehaviour
{
    [SerializeField]
    private GameObject coins;
    private int percent=100;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {   
            Destroy(collision.gameObject);
            Destroy(gameObject);
            var box_drop = 100;//Random.Range(1, 100);
            if (box_drop == percent)
            {
                Instantiate(coins,transform.position,Quaternion.identity);
            }
        }
    }
}
