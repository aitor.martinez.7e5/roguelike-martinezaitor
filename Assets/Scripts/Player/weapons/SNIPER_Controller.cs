using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SNIPER_Controller : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Camera camera;
    [SerializeField]
    GameObject bullet;
    [SerializeField]
    private float bulletForce;

    public float actual_ammo;

    public float max_ammo;
    private bool hasAmmo;
    private bool canShoot;
    public float attack_speed;
    public float attack_size;
    public float attack_damage;




    private void Awake()
    {
        actual_ammo = 15;
        max_ammo = 15;
    }



    void Start()
    {
        hasAmmo = true;
        canShoot = true;
    }


    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("MENU").GetComponent<MENUManager>().isStarted)
        {
            Shoot();
            GunRotation();
            /*  if (actual_ammo == 0)
              {
                  hasAmmo = false;
              }*/
            AmmoComprobation();
        }

    }

    void GunRotation()
    {
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.z = 0;

        Vector3 lookAtDirection = mouseWorldPosition - target.position;
        target.right = lookAtDirection;
        /*
        if (mouseWorldPosition)
        {
            this.GetComponent<SpriteRenderer>().flipX=true;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
        */

    }
    void Shoot()
    {
        if (canShoot && hasAmmo)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
            {
                actual_ammo = actual_ammo - 1;
                var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
                shoot.transform.localScale *= attack_size;
                shoot.transform.position = target.position;
                shoot.GetComponent<Rigidbody2D>().velocity = target.right * bulletForce;
                //shoot.transform.up = Vector2.down;
                StartCoroutine(ShootCooldown());

            }


        }
    }


    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attack_speed);
        canShoot = true;
    }
    private void AmmoComprobation()
    {
        if (actual_ammo == 0)
        {
            hasAmmo = false;
            canShoot = false;
        }
    }
}
