using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController : MonoBehaviour
{
    
    [SerializeField]
    private Camera camera;
    [SerializeField]
    private float velocity;
    private Rigidbody2D rb;
    private float movimientoX;
    private float movimientoY;
<<<<<<< HEAD
    [SerializeField]
    private GameObject gun;
    public float hp = 60;
    private Animator ar;
    private SpriteRenderer gunsr;

=======
    public float hp = 60;
    public bool notDashing;
    public float dashSpeed = 25.0f;
    public float dashCooldown = 10;
    public float dashTimer = 10;
    Vector2 mousePos;
    private BoxCollider2D box;
    private Animator ar;
    public GameObject[] weapons;
    public int _indexWeapon=0;    
    public GameObject _ActualWeapon ;
    public float attack_damage;
    public float actual_ammo;
>>>>>>> main

    public float max_ammo;
    private void Awake()

    {
        attack_damage = 30f;
        actual_ammo = 30;
        max_ammo = 120;
    }


    // Start is called before the first frame update
    void Start()
    {
<<<<<<< HEAD
        ar = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();     
        gunsr = gun.GetComponent<SpriteRenderer>();
=======
       
        _ActualWeapon = weapons[_indexWeapon];
        weapons[0].SetActive(false);
        weapons[1].SetActive(false);
        weapons[2].SetActive(false);
        weapons[3].SetActive(false);
        
        notDashing = true;
        ar = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
>>>>>>> main
    }

    // Update is called once per frame
    void Update()
    { 
        
        Dash();
        WeaponSelection();
    }

    void FixedUpdate()
    {
        if (GameObject.Find("MENU").GetComponent<MENUManager>().isStarted)
        {
         Move();
           
        }
        
        if (dashTimer >= dashCooldown)
        {
           notDashing = true;
        }
            
    }

    void Move()
    {
       
            movimientoX = Input.GetAxisRaw("Horizontal");
            movimientoY = Input.GetAxisRaw("Vertical");
            rb.velocity = new Vector2(movimientoX * velocity, movimientoY * velocity);
            rb.velocity.Normalize();
            if (rb.velocity != Vector2.zero)
            {
                ar.SetFloat("MovimientoX", movimientoX);
                ar.SetFloat("MovimientoY", movimientoY);
                ar.SetBool("Moviendo", true);
            }
            else
            {
                ar.SetBool("Moviendo", false);
            }
        

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("laser"))
        {
            Destroy(collision.gameObject);
            GameObject.Find("HUD").GetComponent<HUDManager>().ReceiveDamage();
        }
       
    }


    void Dash()
    {
        if (notDashing)
        {
            if (Input.GetMouseButtonDown(1))
            {
                box.isTrigger = true;
                mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
                transform.position = mousePos * dashSpeed;
                notDashing = false;
                box.isTrigger = false;
                dashTimer = 0.0f;
            }
        }
        dashTimer += Time.deltaTime; //Makes timer increase
        if (dashTimer >= dashCooldown)
        {
            dashTimer = dashCooldown; //Stops the timer
        }
    }

    void WeaponSelection()
    {
<<<<<<< HEAD
        movimientoX = Input.GetAxisRaw("Horizontal");
        movimientoY = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector2(movimientoX * velocity, movimientoY * velocity);
        rb.velocity.Normalize();
        if (rb.velocity != Vector2.zero)
        {
            ar.SetFloat("MovimientoX", movimientoX);
            ar.SetFloat("MovimientoY",movimientoY);
            ar.SetBool("Moviendo", true);
        }
        else
        {
            ar.SetBool("Moviendo", false);
        }
    }

     
    private void Up()
    {
        gunsr.sortingOrder = 1;
    }

    private void Down()
    {
        gunsr.sortingOrder = 2;
    }
     

    private void Left()
    {
        gunsr.sortingOrder = 1;
    }

    private void Right()
    {
        gunsr.sortingOrder = 1;
    }
}
=======
        weapons[_indexWeapon].SetActive(true);
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {

            weapons[0].SetActive(true);
            weapons[1].SetActive(false);
            weapons[2].SetActive(false);
            weapons[3].SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _indexWeapon = 1;
            weapons[0].SetActive(false);
            weapons[1].SetActive(true);
            weapons[2].SetActive(false);
            weapons[3].SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _indexWeapon = 2;
            weapons[0].SetActive(false);
            weapons[1].SetActive(false);
            weapons[2].SetActive(true);
            weapons[3].SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _indexWeapon = 3;
            weapons[0].SetActive(false);
            weapons[1].SetActive(false);
            weapons[2].SetActive(false);
            weapons[3].SetActive(true);

        }





    }

    }


>>>>>>> main
