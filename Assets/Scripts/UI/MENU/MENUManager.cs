using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MENUManager : MonoBehaviour
{
    public GameObject lose;
    [SerializeField]
    private GameObject hud;
    [SerializeField]
    private GameObject startPanel;
    [SerializeField]
    private Button button;
    public bool isStarted;
    // Start is called before the first frame update
    void Start()
    {
        lose.SetActive(false);
        hud.SetActive(false);
        isStarted = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void StartGame()
    {
        

        startPanel.SetActive(false);
        button.gameObject.SetActive(false);
        hud.SetActive(true);
        isStarted = true;
    }


    public void ClickToStart()
    {
        StartGame();
    }

}
